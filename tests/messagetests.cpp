#include "gtest/gtest.h"

#include "MessageFactory.h"

using namespace common::protocol;

class MessageTest : public ::testing::Test {
protected:
   virtual void SetUp() {
   }

   virtual void TearDown() {
   }
};

TEST_F(MessageTest, TestVerifyConstructorThrows) {
   std::string buffer("Bad message");
   auto isThrown = false;
   try {
       common::protocol::pitch::MessageFactory::createMessage(buffer);
   }
   catch (std::exception) {
       isThrown = true;
   }
   EXPECT_EQ(isThrown, true);
}

TEST_F(MessageTest, TestMessageGetOrderId) {
	std::string buffer("28800011AAK27GA0000DTS000100SH    0000619200Y");
	auto message = pitch::MessageFactory::createMessage(buffer);
	const std::string expectedValue = "AK27GA0000DT";
	auto actualValue = message->getOrderId();
	EXPECT_EQ(expectedValue, actualValue);
}

TEST_F(MessageTest, TestAddOrderMessageGetType) {
	std::string buffer("28800011AAK27GA0000DTS000100SH    0000619200Y");
	auto message = pitch::MessageFactory::createMessage(buffer);
	char expectedValue = pitch::MessageFactory::OrderAdd;
	auto actualValue = message->getType();
	EXPECT_EQ(expectedValue, actualValue);
}

TEST_F(MessageTest, TestAddOrderMessageGetShares) {
	std::string buffer("28800011AAK27GA0000DTS000100SH    0000619200Y");
	auto message = pitch::MessageFactory::createMessage(buffer);
	unsigned long expectedValue = 100;
	auto actualValue = message->getShares();
	EXPECT_EQ(expectedValue, actualValue);
}

TEST_F(MessageTest, TesAddOrderMessagetGetSymbol) {
	std::string buffer("28800011AAK27GA0000DTS000100SH    0000619200Y");
	auto message = pitch::MessageFactory::createMessage(buffer);
	std::string expectedValue = "SH    ";
	auto actualValue = message->getSymbol();
	EXPECT_EQ(expectedValue, actualValue);
}

TEST_F(MessageTest, TestGetSharesSame) {
	std::string buffer("28800011AAK27GA0000DTS000100SH    0000619200Y");
	auto addOrderShares = pitch::MessageFactory::createMessage(buffer)->getShares();

	// message type Trade
	buffer = "28800011PAK27GA0000DTS000100SH    0000619200Y";
	auto tradeShares = pitch::MessageFactory::createMessage(buffer)->getShares();
	EXPECT_EQ(addOrderShares, tradeShares);

	// message type TradeLong
	buffer = "28800011rAK27GA0000DTS000100SH    0000619200Y";
	auto tradeLongShares = pitch::MessageFactory::createMessage(buffer)->getShares();
	EXPECT_EQ(tradeShares, tradeLongShares);
}