//
//  ReportObjectFactory.h
//
#ifndef REPORT_OBJECT_FACTORY_H
#define REPORT_OBJECT_FACTORY_H

#include <memory>
#include <string>

#include "ExecutedVolumeReport.h"
#include "Report.h"

namespace common {
	namespace util {
		class ReportObjectFactory {
		public:
			std::shared_ptr<Report> create(const std::string& reportName, unsigned long count = 10) {
				if (reportName == "exec_vol") {
					return std::shared_ptr<Report>(new ExecutedVolumeReport(count));
				}
				// other reports

				throw std::invalid_argument("No report defined for: " + reportName);
			}
		};
	}
}

#endif

