//
//  OrderExecutedMessage.h
//
#ifndef ORDER_EXECUTED_MESSAGE_H
#define ORDER_EXECUTED_MESSAGE_H

#include <string>
#include "Message.h"

namespace common {
	namespace protocol {
		namespace pitch {
			class OrderExecutedMessage : public Message {
			public:
				explicit OrderExecutedMessage(const std::string& message) : Message(message) {
				}

				unsigned long getShares() const {
					static const unsigned short offset = 21;
					static const unsigned short len = 6;

					return std::stoul(getBufferSlice(offset, len));
				}

				std::string getSymbol() const {
					return "";
				}
			};
		}
	}
}

#endif
