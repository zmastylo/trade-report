//
//  IgnoredMessage.h
//

#ifndef IGNORED_MESSAGE_H
#define IGNORED_MESSAGE_H

#include <string>
#include <stdexcept>
#include "Message.h"

namespace common {
	namespace protocol {
		namespace pitch {
			class IgnoredMessage : public Message {
			public:
				explicit IgnoredMessage(const std::string& message) : Message(message) {
				}

				unsigned long getShares() const {
					throw new std::runtime_error("Ignored message has no shares");
				}

				std::string getSymbol() const {
					throw new std::runtime_error("Ignored message has no symbol");
				}
			};
		}
	}
}

#endif
