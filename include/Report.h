//
//  Report.h
//
#ifndef REPORT_H
#define REPORT_H

namespace common {
	namespace util {

		class Report {
		public:
			static const std::size_t DefaultReportSize = 10;
			
			virtual ~Report() {
			}

			/// aggregate pitch messages
			virtual void operator()(const std::string& message) = 0;

			/// run report
			virtual void operator()() = 0;
		};
	} 
} 

#endif

