//
//  OrderAddMessage.h
//
#ifndef ORDER_ADD_MESSAGE_H
#define ORDER_ADD_MESSAGE_H

#include <string>
#include "Message.h"

namespace common {
	namespace protocol {
		namespace pitch {
			class OrderAddMessage : public Message {
			public:
				explicit OrderAddMessage(const std::string& message)
					: Message(message) {
				}
			};
		}
	}
}

#endif
