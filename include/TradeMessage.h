//
//  TradeMessage.h
//
#ifndef TRADE_MESSAGE_H
#define TRADE_MESSAGE_H

#include <string>
#include "Message.h"

// Trade message
namespace common {
	namespace protocol {
		namespace pitch {
			class TradeMessage : public Message {
			public:
				explicit TradeMessage(const std::string& message) : Message(message) {
				}
			};
		}
	}
}

#endif

