//
//  OrderAddLongMessage.h
//
#ifndef ORDER_ADD_LONG_MESSAGE_H
#define ORDER_ADD_LONG_MESSAGE_H

#include <string>
#include "Message.h"

namespace common {
	namespace protocol {
		namespace pitch {
			class OrderAddLongMessage : public Message {
			public:
				explicit OrderAddLongMessage(const std::string& message)
					: Message(message) {
				}

				std::string getSymbol() const {
					static const unsigned short offset = 28;
					static unsigned short len = 8;

					return getBufferSlice(offset, len);
				}
			};
		}
	}
}

#endif
